package net.iescierva.dam17.pythonsenseplus;

import android.content.SharedPreferences;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentManager;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private static MainActivity myself;

    public static String serverLocal = "10.0.2.2:8000";

    public MainActivity() {
        myself = this;
    }

    public static MainActivity getInstance() {
        return myself;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    //Método para registrar a un usuario
    public boolean SharedPreferencesSet(String nombre, String contraseña){

        Boolean existe = false;

        //Creación de un fichero plano
        SharedPreferences sharpref = getSharedPreferences("dtoLogin", this.MODE_PRIVATE);

        String nombres = sharpref.getString("nombres", "");
        String[] nombresSplit = nombres.split(",");

        for (int i = 0; i < nombresSplit.length; i++){

            String comNom = nombresSplit[i];

            if(comNom.equals(nombre)) {

                existe = true;
                break;
            }
        }

        if (existe == false) {

            String contraseñas = sharpref.getString("contraseñas", "");

            nombres = nombres + nombre + ",";
            contraseñas = contraseñas + contraseña + ",";

            SharedPreferences.Editor editor = sharpref.edit();
            editor.putString("nombres", nombres);
            editor.putString("contraseñas", contraseñas);
            editor.commit();

            Toast.makeText(this, "Usuario creado con exito", Toast.LENGTH_SHORT).show();



        }
        else{

            Toast.makeText(this, "El nombre de usuario ya existe", Toast.LENGTH_SHORT).show();


        }

        return existe;

    }

    //Método para recoger los nombres del fichero generado
    public ArrayList<String> SharedPreferencesGetNombre(){

        ArrayList<String> arrayNombres = new ArrayList<>();

        SharedPreferences sharpref = getSharedPreferences("dtoLogin", this.MODE_PRIVATE);

        String nombres = sharpref.getString("nombres", "");
        String[] nombresSplit = nombres.split(",");

        for (int i = 0; i<nombresSplit.length; i++){

            String addNombre = nombresSplit[i];

            arrayNombres.add(addNombre);

        }

        return arrayNombres;

    }

    //Método para recoger las contraseñas del fichero generado
    public ArrayList<String> SharedPreferencesGetContraseña(){

        ArrayList<String> arrayContraseñas = new ArrayList<>();

        SharedPreferences sharpref = getSharedPreferences("dtoLogin", this.MODE_PRIVATE);

        String contraseñas = sharpref.getString("contraseñas", "");
        String[] contraseñasSplit = contraseñas.split(",");

        for (int i = 0; i<contraseñasSplit.length; i++){

            String addContraseña = contraseñasSplit[i];

            arrayContraseñas.add(addContraseña);

        }

        return arrayContraseñas;

    }


}
