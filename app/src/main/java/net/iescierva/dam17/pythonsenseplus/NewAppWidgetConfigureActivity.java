package net.iescierva.dam17.pythonsenseplus;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static androidx.navigation.Navigation.findNavController;

/**
 * The configuration screen for the {@link NewAppWidget NewAppWidget} AppWidget.
 */
public class NewAppWidgetConfigureActivity extends Activity {

    boolean existe = false;
    String aparato = "";
    String sensor = "";
    String nameValue = "";

    private ProgressDialog progressDialog;

    RequestQueue mQueue = null;
    ArrayList<String> listaNumAparatos = new ArrayList<>();
    ListView listaAparatos;
    ArrayAdapter adaptador;

    JSONArray jsonArray = new JSONArray();

    ArrayList<String> lista = new ArrayList<>();
    ArrayList<String> Sensores = new ArrayList<>();

    private static final String PREFS_NAME = "net.iescierva.dam17.pythonsenseplus.NewAppWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;

    /*

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = NewAppWidgetConfigureActivity.this;

            // When the button is clicked, store the string locally
            String widgetText = sensor;
            saveTitlePref(context, mAppWidgetId, widgetText);

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            NewAppWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };

    */

    public NewAppWidgetConfigureActivity() {
        super();
    }

    // Write the prefix to the SharedPreferences object for this widget
    static void saveTitlePref(Context context, int appWidgetId, String text) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putString(PREF_PREFIX_KEY + appWidgetId, text);
        prefs.apply();
    }

    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource


    static String loadTitlePref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        String titleValue = prefs.getString(PREF_PREFIX_KEY + appWidgetId, null);

        if (titleValue != null) {
            return titleValue;
        } else {
            return context.getString(R.string.appwidget_text);
        }

    }


    static void deleteTitlePref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.apply();
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.

        listaAparatos = (ListView) findViewById(R.id.idLista);
        progressDialog = new ProgressDialog(this);

        setResult(RESULT_CANCELED);

        setContentView(R.layout.new_app_widget_configure);

        //findViewById(R.id.add_button).setOnClickListener(mOnClickListener);

        listaAparatos = findViewById(R.id.idList);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
            return;
        }

        jsonParse();

    }


    public void jsonParse(){

        String server = MainActivity.serverLocal;

        mQueue = Volley.newRequestQueue(getApplicationContext());
        String url ="http://" + server + "/rest/dashboard/devices_names/";

        progressDialog.setMessage("Buscando dispositivos...");
        progressDialog.show();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            nombre = jsonObject.getString("Device");

                            listaNumAparatos.add(nombre);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, listaNumAparatos);

                    listaAparatos.setAdapter(adaptador);


                    progressDialog.dismiss();

                    listaAparatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            try {

                                aparato = listaNumAparatos.get(position);

                                adaptador.clear();

                                Sensores(aparato);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }

    private void Sensores(final String aparato) {

        String server = MainActivity.serverLocal;

        mQueue = Volley.newRequestQueue(getApplicationContext());
        String url ="http://" + server + "/rest/" + aparato + "/sensor_names/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            nombre = jsonArray.getString(i);

                            if (!nombre.equals("GPS")) {
                                Sensores.add(nombre);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, Sensores);

                    listaAparatos.setAdapter(adaptador);

                    listaAparatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            try {

                                sensor = Sensores.get(position);

                                adaptador.clear();

                                elegirValor(aparato, sensor);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }



    public void elegirValor(String device, final String sensor){

        String server = MainActivity.serverLocal;

        mQueue = Volley.newRequestQueue(getApplicationContext());
        String url = "http://" + server + "/rest/" + device + "/" + sensor + "/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    for(int i = 0; i < jsonArray.length(); i++){


                        try {

                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            nameValue = jsonObject.getString("Name");



                            for (int j = 0; j < lista.size(); j++) {

                                if (lista.get(j).equals(nameValue)) {

                                    existe = true;
                                    break;

                                }
                            }

                            if (existe == false) {

                                lista.add(nameValue);

                            }

                            

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    final ArrayAdapter adaptador = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, lista);

                    listaAparatos.setAdapter(adaptador);

                    listaAparatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                            new Thread(new Runnable() {
                                public void run() {
                                    try {
                                        while(true){
                                            Valores(aparato, sensor, position);
                                            Thread.sleep(6000);
                                        }

                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).start();

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }




    public void Valores(final String aparato, final String sensor, final int posicion) {

        String server = MainActivity.serverLocal;

        mQueue = (RequestQueue) Volley.newRequestQueue(this);
        String url = "http://" + server + "/rest/" + aparato + "/" + sensor + "/";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @SuppressLint("LongLogTag")
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    JSONObject jsonObject = (JSONObject) jsonArray.get(posicion);

                    String value = jsonObject.getString("Value");
                    String nombre = jsonObject.getString("Name");

                    String texto = nombre + " del sensor " + sensor + " del dispositivo " + aparato + ": " + value;

                    //Log.e(texto, "TEXTO");

                    final Context context = NewAppWidgetConfigureActivity.this;

                    // When the button is clicked, store the string locally

                    saveTitlePref(context, mAppWidgetId, texto);

                    // It is the responsibility of the configuration activity to update the app widget
                    AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
                    NewAppWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

                    // Make sure we pass back the original appWidgetId
                    Intent resultValue = new Intent();
                    resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
                    setResult(RESULT_OK, resultValue);

                    finish();




                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);


    }


}