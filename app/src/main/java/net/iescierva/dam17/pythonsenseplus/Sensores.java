package net.iescierva.dam17.pythonsenseplus;


import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
//import android.support.v4.app.Fragment;
//import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.fragment.app.Fragment;


/**
 * A simple {@link Fragment} subclass.
 */
public class Sensores extends Fragment {

    Bundle recibir = new Bundle();

    boolean continuar = true;

    int posicionValor;

    String sensor = "";
    String device = "";
    String value = "";
    String nombre = "";
    String date = "";

    String value2;
    double num2;
    String date2 = "";

    String value3;
    double num3;
    String date3 = "";

    double num = 0;

    JSONArray jsonArray = new JSONArray();
    RequestQueue mQueue;

    TextView tvSensor;

    TextView tvDate1;
    TextView tvValue1;
    ProgressBar progressBar1;

    TextView tvDate2;
    TextView tvValue2;
    ProgressBar progressBar2;

    TextView tvDate3;
    TextView tvValue3;
    ProgressBar progressBar3;

    Handler handler = new Handler();

    Thread hilo;

    public Sensores() {
        // Required empty public constructor
    }

    @Override
    public void onDestroy() {
        hilo.interrupt();
        super.onDestroy();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_sensores, container, false);

        tvSensor = (TextView) vista.findViewById(R.id.idSensor);

        tvDate1 = (TextView) vista.findViewById(R.id.idDate1);
        tvValue1 = (TextView) vista.findViewById(R.id.idValue1);
        progressBar1 = (ProgressBar) vista.findViewById(R.id.progressBar1);

        tvDate2 = (TextView) vista.findViewById(R.id.idDate2);
        tvValue2 = (TextView) vista.findViewById(R.id.idValue2);
        progressBar2 = (ProgressBar) vista.findViewById(R.id.progressBar2);

        tvDate3 = (TextView) vista.findViewById(R.id.idDate3);
        tvValue3 = (TextView) vista.findViewById(R.id.idValue3);
        progressBar3 = (ProgressBar) vista.findViewById(R.id.progressBar3);

        //método para recibir el Bundle del Fragment anterior
        recibir = getArguments();
        sensor = recibir.getString("sensor");
        device = recibir.getString("device");
        posicionValor = recibir.getInt("posicionValor");

        //Thread con bucle infinito que llama al método Valores
        hilo = new Thread(new Runnable() {
            public void run() {
                try {
                    while(continuar){
                        Valores();
                        Thread.sleep(5000);
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        hilo.start();

        return vista;

    }

    //Metodo para realizar una petición al servidor
    private void Valores() {

        String server = MainActivity.serverLocal;

        mQueue = Volley.newRequestQueue(getContext());
        String url = "http://" + server + "/rest/" + device + "/" + sensor + "/";

        //petición al servidor
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    //jsonObject con el valor seleccionado
                    JSONObject jsonObject = (JSONObject) jsonArray.get(posicionValor);

                    //igualaciones para pasar los valores entre ellas
                    value3 = value2;
                    value2 = value;
                    num3 = num2;
                    num2 = num;
                    date3 = date2;
                    date2 = date;

                    //sacar datos del jsonObject
                    nombre = jsonObject.getString("Name");
                    value = jsonObject.getString("Value");
                    date = jsonObject.getString("Date");

                    num = Double.parseDouble(value);

                    //setear valores en los TextViews
                    tvSensor.setText(nombre);

                    tvDate1.setText(date);
                    tvDate2.setText(date2);
                    tvDate3.setText(date3);

                    tvValue1.setText(value);
                    tvValue2.setText(value2);
                    tvValue3.setText(value3);


                    //Thread donde se modifica el color de la progressBar1
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        progressBar1.getProgressDrawable().setColorFilter(
                                                Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN);

                                        if (nombre.equals("Temperature") & (int)num >= 40){

                                            progressBar1.getProgressDrawable().setColorFilter(
                                                    Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
                                        }

                                        progressBar1.setProgress((int)num);
                                    }
                                });

                        }
                    }).start();

                    //Thread donde se modifica el color de la progressBar2
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    progressBar2.getProgressDrawable().setColorFilter(
                                            Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN);


                                    if (nombre.equals("Temperature") & (int)num2 >= 40){

                                        progressBar2.getProgressDrawable().setColorFilter(
                                                Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);

                                    }

                                    progressBar2.setProgress((int)num2);

                                }
                            });

                        }
                    }).start();

                    //Thread donde se modifica el color de la progressBar3
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    progressBar3.getProgressDrawable().setColorFilter(
                                            Color.BLUE, android.graphics.PorterDuff.Mode.SRC_IN);


                                    if (nombre.equals("Temperature") & (int)num3 >= 40){

                                        progressBar3.getProgressDrawable().setColorFilter(
                                                Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);

                                    }

                                    progressBar3.setProgress((int)num3);

                                }
                            });

                        }
                    }).start();

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }

}