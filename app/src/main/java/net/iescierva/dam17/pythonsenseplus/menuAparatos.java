package net.iescierva.dam17.pythonsenseplus;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import static androidx.navigation.Navigation.findNavController;

public class menuAparatos extends Fragment {
    RequestQueue mQueue = null;

    public menuAparatos() {
        // Required empty public constructor
    }

    ArrayList<String>listaNumAparatos = new ArrayList<>();
    ListView listaAparatos;
    JSONArray jsonArray = new JSONArray();

    AlertDialog.Builder alerta;

    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View vista = inflater.inflate(R.layout.fragment_menu_aparatos, container, false);

        listaAparatos = (ListView) vista.findViewById(R.id.idLista);
        progressDialog = new ProgressDialog(getContext());

        alerta = new AlertDialog.Builder(getContext());
        alerta.setTitle("ERROR");
        alerta.setMessage("No se ha podido establecer conexión con el servidor");
        alerta.setCancelable(false);
        alerta.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogo1, int id) {

                        findNavController(vista).navigate(R.id.idAtras);

                    }
                    });

                    //llamada al método jsonParse
        jsonParse();

        return vista;
    }


    //Metodo para realizar una petición al servidor
    public void jsonParse(){

        String server = MainActivity.serverLocal;

        mQueue = Volley.newRequestQueue(getContext());
        String url ="http://" + server + "/rest/dashboard/devices_names/";

        //pantallita de carga
        progressDialog.setMessage("Buscando dispositivos...");
        progressDialog.show();

        //petición al servidor
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    //for para recorrer la jsonArray
                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            nombre = jsonObject.getString("Device");

                            listaNumAparatos.add(nombre);
                        } catch (Exception e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }


                    }

                    //Adaptador para la inserción de datos en una ListView
                    final ArrayAdapter adaptador = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, listaNumAparatos);

                    listaAparatos.setAdapter(adaptador);

                    progressDialog.dismiss();

                    //método OnClick de la ListView
                    listaAparatos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            try {

                                String device = listaNumAparatos.get(position);

                                adaptador.clear();

                                Bundle bundle = new Bundle();
                                bundle.putString("device", device);

                                //cambiar al siguiente Fragment pasandole información
                                findNavController(view).navigate(R.id.idMenuSensores, bundle);

                            } catch (Exception e) {
                                e.printStackTrace();
                                progressDialog.dismiss();
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    progressDialog.dismiss();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

                progressDialog.dismiss();
                alerta.show();
                //Toast.makeText(getContext(), "tiempo de espera expirado", Toast.LENGTH_SHORT).show();

            }
        });

        mQueue.add(request);



    }



}
