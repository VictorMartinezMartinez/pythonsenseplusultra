package net.iescierva.dam17.pythonsenseplus;

import android.os.Bundle;
//import android.support.v4.app.Fragment;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import petrov.kristiyan.colorpicker.ColorPicker;

import static androidx.navigation.Navigation.findNavController;

public class menuSensores extends Fragment {

    String url;
    String colour;

    JSONArray jsonArray = new JSONArray();

    RecyclerView recyclerSensores;
    sensoresAdapter adapter;


    FloatingActionButton fab;

    TextView tvDevice;
    RequestQueue mQueue = null;

    Bundle recibir = new Bundle();
    String device;

    ArrayList<String> Sensores = new ArrayList<>();

    ArrayList<recyclerSensores> listaSensores;

    public menuSensores() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View vista = inflater.inflate(R.layout.fragment_menu, container, false);

        tvDevice = (TextView) vista.findViewById(R.id.tvDevide);
        recyclerSensores = (RecyclerView) vista.findViewById(R.id.recyclerId);
        recyclerSensores.setLayoutManager(new LinearLayoutManager(getContext()));
        //floatingActionMenu.setClosedOnTouchOutside(true);

        fab = (FloatingActionButton) vista.findViewById(R.id.idButton);

        listaSensores = new ArrayList<>();

        //método para recibir el Bundle del Fragment anterior
        recibir = getArguments();
        device = recibir.getString("device");
        tvDevice.setText(device);

        Sensores();

        Sensores.clear();

        //metodo OnClick para el FloatingActionButton
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ColorPicker colorPicker = new ColorPicker(getActivity());
                final ArrayList<String> colores = new ArrayList<>();
                //colores.add("#FFFFFFFF"); // w
                colores.add("#FF0000"); // red
                colores.add("#00FF00"); // green
                colores.add("#0000FF"); // blue
                colores.add("#4E0041"); // violet
                colores.add("#B9935A"); // ochre
                colores.add("#6AF7FE"); // celeste
                colores.add("#FE33F2"); // pink
                colores.add("#FFFFFF"); // white
                colores.add("#000000"); // black
                //colores.add("#123456"); // random
                //colorPicker.setDefaultColorButton(Color.parseColor("#FFFFFF"));
                colorPicker.setColors(colores);
                colorPicker.setColumns(3);
                colorPicker.setRoundColorButton(true);
                colorPicker.setOnChooseColorListener(new ColorPicker.OnChooseColorListener() {
                    @Override
                    public void onChooseColor(int position, int color) {

                        String c_olor = "#000000";

                        try {
                            colour = colores.get(position);

                        //Toast.makeText(getApplicationContext(), colour, Toast.LENGTH_SHORT).show();
                        // variable que envia el valor del color
                        int [] arrayEnteros = new int [colores.size()];
                        for (int i = 0; i < colores.size(); i++){
                            arrayEnteros[i] = i;
                        }
                        c_olor = colour;
                        Log.d("Que c_olor es ", c_olor);

                        }catch (Exception e){}
                        try {
                            int colorin = -1;
                            if (c_olor.equals("#FF0000")) {
                                colorin = 0; // color rojo
                            } else if (c_olor.equals("#00FF00")) {
                                colorin = 1; // color verde
                            } else if (c_olor.equals("#0000FF")) {
                                colorin = 2; // color azul
                            } else if (c_olor.equals("#FFFFFF")) {
                                colorin = 3; // encendido normal
                            } else if (c_olor.equals("#000000")) {
                                colorin = 4; // apagado
                            } else if (c_olor.equals("#4E0041")) {
                                colorin = 5;
                            } else if (c_olor.equals("#B9935A")) {
                                colorin = 6;
                            } else if (c_olor.equals("#6AF7FE")) {
                                colorin = 7;
                            } else if (c_olor.equals("#FE33F2")) {
                                colorin = 8;
                            }

                            sendPost(colorin);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }

                    @Override
                    public void onCancel() {

                    }
                });
                colorPicker.show();

            }
        });


        return vista;

    }

    //Metodo para realizar una petición al servidor
    private void Sensores() {

        String server = MainActivity.serverLocal;

        mQueue = Volley.newRequestQueue(getContext());
        String url ="http://" + server + "/rest/" + device + "/sensor_names/";

        //petición al servidor
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    //for para recorrer la jsonArray
                    for(int i = 0; i < jsonArray.length(); i++){

                        String nombre = null;
                        try {

                            nombre = jsonArray.getString(i);

                            Sensores.add(nombre);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    //for para recorrer array Sensores y elegir imagen a introducir

                    for (int i = 0; i < Sensores.size(); i++){

                        if (Sensores.get(i).equals("Temperature") | Sensores.get(i).equals("Temperatura")) {

                            listaSensores.add(new recyclerSensores(Sensores.get(i), R.mipmap.temperatura));

                        }

                        if (Sensores.get(i).equals("Battery") | Sensores.get(i).equals("Bateria")) {

                            listaSensores.add(new recyclerSensores(Sensores.get(i), R.mipmap.bateria));

                        }

                        if (Sensores.get(i).equals("Light") | Sensores.get(i).equals("Luz")) {

                            listaSensores.add(new recyclerSensores(Sensores.get(i), R.mipmap.luz));

                        }

                        if (Sensores.get(i).equals("Barometer") | Sensores.get(i).equals("Barometro")) {

                            listaSensores.add(new recyclerSensores(Sensores.get(i), R.mipmap.presion));

                        }

                    }

                    //objeto del adaptador para la RecyclerView
                    adapter = new sensoresAdapter(listaSensores);

                    //método OnClick de la RecyclerView
                    adapter.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String sensor = listaSensores.get(recyclerSensores.getChildAdapterPosition(v)).getSensor();

                            Bundle bundle = new Bundle();
                            bundle.putString("sensor", sensor);
                            bundle.putString("device", device);

                            findNavController(v).navigate(R.id.idListValores, bundle);

                        }
                    });

                    recyclerSensores.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }


    //método para realizar Post al servidor
    public void sendPost(int colorin) throws JSONException {

        String server = MainActivity.serverLocal;

        url = "http://" + server + "/rest/" + device + "/actions/";

        Log.d("Dentro del sendPost", String.valueOf(colorin));
        mQueue = Volley.newRequestQueue(getContext());
        JSONObject jsonObj = new JSONObject();

        JSONArray jsonArraySens = new JSONArray();
        JSONArray jsonArrayMeas = new JSONArray();
        JSONArray jsonArrayAct = new JSONArray();
        JSONObject jsonItem = new JSONObject();
        JSONObject jsonItemMeas = new JSONObject();
        JSONObject jsonItemAct = new JSONObject();

        //------------------------------------------
        jsonObj.put("id",device);
        //------------------------------------------
        //------------------------------------------
        //-----array de measures
        /*jsonItemMeas.put("name", "Color");
        jsonItemMeas.put("value", "");
        jsonItemMeas.put("magnitude", "color");
        //----------- "measures":[{...
        jsonArrayMeas.put(jsonItemMeas);*/
        //------------------------------------------
        //--------------array de actions
        jsonItemAct.put("name","Luz");
        jsonItemAct.put("state",colorin);
        jsonArrayAct.put(jsonItemAct);
        //--------------"sensors": [{....
        jsonObj.put("actions",jsonArrayAct);
        //jsonObj.put("sensors",jsonArrayMeas);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST, url, jsonObj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // Toast.makeText(getApplicationContext(), response.toString(), Toast.LENGTH_SHORT).show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Toast.makeText(getApplicationContext(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        Log.d("Dentro del sendPost", jsonObj.toString());
        mQueue.add(jsonObjReq);

    }

}