package net.iescierva.dam17.pythonsenseplus;


import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import androidx.fragment.app.Fragment;

import static androidx.navigation.Navigation.findNavController;

public class menuValores extends Fragment {

    ListView listValores;

    boolean existe = false;
    String device;
    String sensor;

    String nameValue;
    ArrayList<String> lista = new ArrayList<>();

    Bundle recibir = new Bundle();

    RequestQueue mQueue = null;
    JSONArray jsonArray;

    public menuValores() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_menu_valores, container, false);

        listValores = (ListView) vista.findViewById(R.id.idListValores);

        //método para recibir el Bundle del Fragment anterior
        recibir = getArguments();
        device = recibir.getString("device");
        sensor = recibir.getString("sensor");

        elegirValor();

        return vista;
    }


    //Metodo para realizar una petición al servidor
    public void elegirValor(){

        String server = MainActivity.serverLocal;

        mQueue = Volley.newRequestQueue(getContext());
        String url = "http://" + server + "/rest/" + device + "/" + sensor + "/";

        //petición al servidor
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                try {

                    jsonArray = response.getJSONArray("data");

                    //for para recorrer la jsonArray
                    for(int i = 0; i < jsonArray.length(); i++){

                        existe = false;

                        try {

                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            nameValue = jsonObject.getString("Name");

                            for (int j = 0; j < lista.size(); j++) {

                                if (lista.get(j).equals(nameValue)) {

                                    existe = true;
                                    break;

                                }
                            }

                                if (existe == false) {

                                    lista.add(nameValue);

                                }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                    }

                    //Adaptador para la inserción de datos en una ListView
                    final ArrayAdapter adaptador = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, lista);

                    listValores.setAdapter(adaptador);

                    //método OnClick de la ListView
                    listValores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            try {

                                adaptador.clear();

                                Bundle bundle = new Bundle();
                                bundle.putInt("posicionValor", position);
                                bundle.putString("sensor", sensor);
                                bundle.putString("device", device);

                                findNavController(view).navigate(R.id.idValores, bundle);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();

            }
        });

        mQueue.add(request);

    }

}
