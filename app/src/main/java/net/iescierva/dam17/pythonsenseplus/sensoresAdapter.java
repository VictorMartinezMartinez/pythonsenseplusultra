package net.iescierva.dam17.pythonsenseplus;

//import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;

public class sensoresAdapter extends RecyclerView.Adapter<sensoresAdapter.SensorViewHolder>
implements View.OnClickListener{

    ArrayList<recyclerSensores> listaSensores;
    View.OnClickListener listener;

    public sensoresAdapter(ArrayList<recyclerSensores> listaSensor) {
        this.listaSensores=listaSensor;
    }

    @Override
    public SensorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list,null,false);
        view.setOnClickListener(this);

        return new SensorViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SensorViewHolder holder, int position) {
        holder.txtSensores.setText(listaSensores.get(position).getSensor());
        holder.foto.setImageResource(listaSensores.get(position).getImagenId());
    }

    @Override
    public int getItemCount() {
        return listaSensores.size();
    }

    public void setOnClickListener(View.OnClickListener listener){

        this.listener = listener;

    }

    @Override
    public void onClick(View v) {

        if (listener != null){

            listener.onClick(v);

        }

    }

    public class SensorViewHolder extends RecyclerView.ViewHolder {
        TextView txtSensores;
        ImageView foto;

        public SensorViewHolder(View itemView) {
            super(itemView);
            txtSensores= (TextView) itemView.findViewById(R.id.idNombre);
            foto= (ImageView) itemView.findViewById(R.id.idImagen);
        }
    }
}